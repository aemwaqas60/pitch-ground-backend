class BusinessModel {
    businessModelId: string;
    value: string;
    constructor(businessModelId: string, value: string) {

        this.businessModelId = businessModelId;
        this.value = value;
    }

    toObject(): JSON {
        return JSON.parse(JSON.stringify(this));
    };


    static createFromDetails(businessModelId: string, value: string): BusinessModel {

        return new BusinessModel(
            businessModelId,
            value,
        );
    }

    static createFromObject(businessModelOjb: any): BusinessModel {
        return new BusinessModel(
            businessModelOjb.businessModelId,
            businessModelOjb.value,
        );

    }

}

export default BusinessModel;
