import BusinessModel from "./BusinessModel";
interface BusinessModelRepository {
    getByProductId(productId: string): Promise<BusinessModel[] | boolean>;

    create(businessModels: BusinessModel[], productId: string): Promise<boolean>;

    remove(productId: string): Promise<boolean>
}

export default BusinessModelRepository;