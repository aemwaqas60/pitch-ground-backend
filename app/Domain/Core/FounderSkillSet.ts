enum FounderSkillSet {
    foundersCode = 'Founders Code',
    founderDontCode = 'Founders Don\'t Code'
}

export default FounderSkillSet;