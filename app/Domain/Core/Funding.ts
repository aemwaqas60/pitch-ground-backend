class Funding {
    fundingId: string;
    value: string;

    constructor(fundingId: string, value: string) {

        this.fundingId = fundingId;
        this.value = value;

    }

    toObject(): JSON {
        return JSON.parse(JSON.stringify(this));
    };

    static createFromDetails(fundingId: string, value: string): Funding {

        return new Funding(
            fundingId,
            value,
        );
    }

    static createFromObject(fundingOjb: any): Funding {
        return new Funding(
            fundingOjb.fundingId,
            fundingOjb.value,
        );

    }

}

export default Funding;



