import Funding from "./Funding";
interface FundingRepository {
    getByProductId(productId: string): Promise<Funding[] | boolean>;

    create(productFundings: Funding[], productId: string): Promise<boolean>;

    remove(productId: string): Promise<boolean>;

}

export default FundingRepository;