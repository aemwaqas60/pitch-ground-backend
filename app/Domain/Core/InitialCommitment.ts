enum InitialCommitment {
    fullTime = 'Full Time',
    sideProject = 'Side Project'
}

export default InitialCommitment;