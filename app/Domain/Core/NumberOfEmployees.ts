enum NumberOfEmployees {
    tenPlusEmployees = '10+ Employees',
    fiftyPlusEmployees = '50+ Employees',
    twoHundredPlusEmployees = '200+ Employees',
    fiveHundredPlusEmployees = '500+ Employees',
    oneThousandPlusEmployees = '1,000+ Employees',
    fiveThousandPlusEmployees = '5,000+ Employees',
    tenThousandPlusEmployees = '10,000+ Employees',
    noEmployees = 'No Employees',
    underTenEmployees = 'Under 10 Employees',
}

export default NumberOfEmployees;