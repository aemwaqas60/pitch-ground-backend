enum NumberOfFounder {
    multipleFounders = 'Multiple Founder',
    soloFounder = 'Solo Founder'
}

export default NumberOfFounder;