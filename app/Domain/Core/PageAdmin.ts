import User from "./User";

class PageAdmin {
    pageAdminId: string;
    productId: string;
    userId: string;
    user: User;
    role: string;

    constructor(pageAdminId: string, productId: string, userId: string, role: string) {
        this.pageAdminId = pageAdminId;
        this.productId = productId;
        this.userId = userId;
        this.role = role;
    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    };

    /**
     * Set Page Admin User
     * @param {User} user
     */
    setUser(user: User){
        this.user = user;
    }

    toStoreObject(): object{
        return {
            pageAdminId: this.pageAdminId,
            productId: this.productId,
            userId: this.userId,
            role: this.role,
        }
    }

    static createFromDetails(pageAdminId: string, productId: string, userId: string, role: string): PageAdmin {

        return new PageAdmin(
            pageAdminId,
            productId,
            userId,
            role,
        );
    }

    static createFromObject(pageAdminObj: any): PageAdmin {
        return new PageAdmin(
            pageAdminObj.pageAdminId,
            pageAdminObj.productId,
            pageAdminObj.userId,
            pageAdminObj.role,
        );
    }

}


export default PageAdmin;


