import PageAdmin from "./PageAdmin";

interface PageAdminRepository {
    getByProductId(productId: string): Promise<PageAdmin[] | boolean>;

    create(productPageAdmin: PageAdmin[]): Promise<boolean>;

    remove(productId: string): Promise<boolean>;

}

export default PageAdminRepository;