class Platform {
    platformId: string;
    value: string;
    constructor(platformId: string, value: string) {
        this.platformId = platformId;
        this.value = value;
    }


    static createFromDetails(platformId: string, value: string): Platform {

        return new Platform(
            platformId,
            value,
        );
    }


    static createFromObject(platformsOjb: any): Platform {
        return new Platform(
            platformsOjb.platformId,
            platformsOjb.value,
        );

    }

    toObject(): JSON {
        return JSON.parse(JSON.stringify(this));
    };


}

export default Platform;


