import Platform from "./Platform";

interface PlatformRepository {
    getByProductId(productId: string): Promise<Platform[] | boolean>;

    create(productPlatforms: Platform[], productId: string): Promise<boolean>;

    remove(productId: string): Promise<boolean>;

}

export default PlatformRepository;