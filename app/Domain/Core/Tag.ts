class Tag {
    tagId: string;
    value: string;

    constructor(tagId: string, value: string) {

        this.tagId = tagId;
        this.value = value;

    }

    toObject(): JSON {
        return JSON.parse(JSON.stringify(this));
    };

    static createFromDetails(tagId: string, value: string): Tag {

        return new Tag(
            tagId,
            value,
        );
    }

    static createFromObject(tagsOjb: any): Tag {
        return new Tag(
            tagsOjb.tagId,
            tagsOjb.value,
        );

    }

}


export default Tag;
