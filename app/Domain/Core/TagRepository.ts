import Tag from "./Tag";

interface TagRepository {
    getByProductId(productId: string): Promise<Tag[] | boolean>;

    create(productTags: Tag[], productId: string): Promise<boolean>;

    remove(productId: string): Promise<boolean>;
}

export default TagRepository;