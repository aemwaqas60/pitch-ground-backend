class User {
    userId: string;
    fullName: string;
    userName: string;
    email: string;
    password: string;
    publicEmail: string;
    twitterLink: string;
    dateOfBirth: Date;
    location: string;
    profilePic: string;
    biography: string;

    constructor(
        userId: string,
        fullName: string,
        userName: string,
        email: string,
        publicEmail: string,
        twitterLink: string,
        dateOfBirth: Date,
        location: string,
        profilePic: string,
        biography: string
    ) {
        this.userId = userId;
        this.fullName = fullName;
        this.userName = userName;
        this.email = email;
        this.publicEmail = publicEmail;
        this.twitterLink = twitterLink;
        this.dateOfBirth = dateOfBirth;
        this.location = location;
        this.profilePic = profilePic;
        this.biography = biography;
    }

    setPassword(password: string): void {
        this.password = password;
    }

    static createFromObject(userObj: any): User {
        return new User(
            userObj.userId,
            userObj.fullName,
            userObj.userName,
            userObj.email,
            userObj.publicEmail,
            userObj.twitterLink,
            userObj.dateOfBirth,
            userObj.location,
            userObj.profilePic,
            userObj.biography
        );
    }
}

export default User;