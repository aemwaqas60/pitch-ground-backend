class UserFollowingEntity {
    followingId: string;
    userId: string;
    followerId: string;
    constructor(followingId, userId, followerId) {

        this.followingId = followingId;
        this.userId = userId;
        this.followerId = followerId;

    }

    static createFromDetails(followingId, userId, followerId) {

        return new UserFollowingEntity(
            followingId,
            userId,
            followerId,
        );
    }

    static createFromObject(userOjb) {
        return new UserFollowingEntity(
            userOjb.followingId,
            userOjb.userId,
            userOjb.followerId,
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    };


}


module.exports = UserFollowingEntity;


