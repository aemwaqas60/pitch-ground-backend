class UserSettingEntity {
    settingId: string;
    userId: string;
    followersAlerts: string;
    postsAlerts: string;
    followersEmail: string;
    postsEmail: string;

    constructor(settingId, userId, followersAlerts, postsAlerts, followersEmail,
                postsEmail) {

        this.settingId = settingId;
        this.userId = userId;
        this.followersAlerts = followersAlerts;
        this.postsAlerts = postsAlerts;
        this.followersEmail = followersEmail;
        this.postsEmail = postsEmail;

    }


    static createFromDetails(settingId, userId, followersAlerts, postsAlerts, followersEmail,
                             postsEmail) {

        return new UserSettingEntity(
            settingId,
            userId,
            followersAlerts,
            postsAlerts,
            followersEmail,
            postsEmail
        );
    }


    static createFromObject(userOjb) {
        return new UserSettingEntity(
            userOjb.settingId,
            userOjb.userId,
            userOjb.followersAlerts,
            userOjb.postsAlerts,
            userOjb.followersEmail,
            userOjb.postsEmail,
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));

    };


}


module.exports = UserSettingEntity;

