class DiscussionCommentLikesEntity {
    likeId: string;
    commentId: string;
    likes: string;
    constructor(likeId, commentId,likes) {

        this.likeId = likeId;
        this.commentId = commentId;
        this.likes = likes;
    }


    static createFromDetails(likeId,commentId,likes) {

        return new DiscussionCommentLikesEntity(
            likeId,
            commentId,
            likes
        );
    }

    static createFromObject(discussionCommentLikesObj) {
        return new DiscussionCommentLikesEntity(
            discussionCommentLikesObj.likeId,
            discussionCommentLikesObj.commentId,
            discussionCommentLikesObj.likes
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    };

}


module.exports = DiscussionCommentLikesEntity;
