import User from "../Core/User";

class Discussion {
    discussionId: string;
    title: string;
    discussion: string;
    user: User;
    userId: string;
    constructor(discussionId: string, discussion: string, title: string) {
        this.discussionId = discussionId;
        this.discussion = discussion;
        this.title = title;
    }

    setUser(user: User): void {
        this.user = user;
    }

    setUserId(userId: string): void {
        this.userId = userId;
    }

    toObject(): JSON {
        return JSON.parse(JSON.stringify(this));
    };

    toStoreObject(): object {
        return {
            discussionId: this.discussionId,
            title: this.title,
            discussion: this.discussion,
            userId: this.userId,
        }
    }

    static createFromDetails(discussionId: string, discussion: string, title: string): Discussion {

        return new Discussion(
            discussionId,
            discussion,
            title,
        );
    }

    static createFromObject(discussionObj: any): Discussion {
        return new Discussion(
            discussionObj.discussionId,
            discussionObj.discussion,
            discussionObj.title,
        );

    }
}

export default Discussion;
