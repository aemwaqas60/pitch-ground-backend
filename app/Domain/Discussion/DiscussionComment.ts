class DiscussionComment {
    discussionCommentId: string;
    discussionId: string;
    parentDiscussionCommentId: string;
    comment: string;

    constructor(discussionCommentId: string, discussionId: string, parentDiscussionCommentId: string, comment: string) {

        this.discussionCommentId = discussionCommentId;
        this.discussionId = discussionId;
        this.parentDiscussionCommentId = parentDiscussionCommentId;
        this.comment = comment;
    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    };

    static createFromDetails(discussionCommentId: string, discussionId: string, parentDiscussionCommentId: string, comment: string) {

        return new DiscussionComment(
            discussionCommentId,
            discussionId,
            parentDiscussionCommentId,
            comment
        );
    }


    static createFromObject(discussionCommentObj: any) {
        return new DiscussionComment(
            discussionCommentObj.discussionCommentId,
            discussionCommentObj.discussionId,
            discussionCommentObj.parentDiscussionCommentId,
            discussionCommentObj.comment,
        );

    }

}

export default DiscussionComment;
