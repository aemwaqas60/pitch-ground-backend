import DiscussionComment from "./DiscussionComment";

interface DiscussionCommentRepository {
    /**
     *
     * @param discussionComment
     */
    save(discussionComment: DiscussionComment): Promise<boolean>;

    /**
     *
     * @param discussionId
     */
    getByDiscussionId(discussionId: string): Promise<DiscussionComment[]>;

    /**
     *
     * @param discussionComment
     */
    update(discussionComment: DiscussionComment): Promise<boolean>;

    /**
     *
     * @param discussionComment
     */
    remove(discussionComment: DiscussionComment): Promise<boolean>;

}

export default DiscussionCommentRepository;