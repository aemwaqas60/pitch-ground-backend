class DiscussionLikesEntity {
    likesId: string;
    discussionId: string;
    likes: string;
    constructor(likesId, discussionId,likes) {

        this.likesId = likesId;
        this.discussionId = discussionId;
        this.likes = likes;
    }


    static createFromDetails(likesId,discussionId,likes) {

        return new DiscussionLikesEntity(
            likesId,
            discussionId,
            likes
        );
    }


    static createFromObject(discussionLikesObj) {
        return new DiscussionLikesEntity(
            discussionLikesObj.likesId,
            discussionLikesObj.discussionId,
            discussionLikesObj.likes,
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    };

}


module.exports = DiscussionLikesEntity;
