import Discussion from "./Discussion";

interface DiscussionRepository {
    /**
     *
     * @param {Discussion} discussion
     * @param userId
     */
    save(discussion: Discussion, userId: string): Promise<boolean>;

    /**
     * Get All Discussions
     */
    getAll(): Promise<Discussion[]>;

    /**
     *
     * @param userId
     */
    getByUserId(userId: string): Promise<Discussion[]>;

    /**
     *
     * @param discussionId
     * @param userId
     */
    getByDiscussionAndUserId(discussionId: string, userId: string): Promise<Discussion>;

    /**
     *
     * @param {Discussion} discussion
     */
    update(discussion: Discussion): Promise<boolean>;

    /**
     *
     * @param discussion
     */
    remove(discussion: Discussion): Promise<boolean>;

    /**
     *
     * @param userId
     */
    removeAll(userId: string): Promise<boolean>;
}

export default DiscussionRepository;