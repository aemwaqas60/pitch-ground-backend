class HashTagsEntity {
    tagId: string;
    tag: string;
    discussionId: string;
    constructor(tagId, tag,discussionId) {

        this.tagId = tagId;
        this.tag = tag;
        this.discussionId = discussionId;
    }


    static createFromDetails(tagId,tag,discussionId) {

        return new HashTagsEntity(
            tagId,
            tag,
            discussionId
        );
    }


    static createFromObject(hashTagObj) {
        return new HashTagsEntity(
            hashTagObj.tagId,
            hashTagObj.tag,
            hashTagObj.discussionId,
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    };

}


module.exports = HashTagsEntity;
