class HashTagsFollowingEntity {
    followingId: string;
    hashTagId: string;
    userId: string;
    constructor(followingId, hashTagId,userId) {

        this.followingId = followingId;
        this.hashTagId = hashTagId;
        this.userId = userId;
    }


    static createFromDetails(followingId,hashTagId,userId) {

        return new HashTagsFollowingEntity(
            followingId,
            hashTagId,
            userId
        );
    }


    static createFromObject(hashTagFollowingObj) {
        return new HashTagsFollowingEntity(
            hashTagFollowingObj.followingId,
            hashTagFollowingObj.hashTagId,
            hashTagFollowingObj.userId,
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    };

}


module.exports = HashTagsFollowingEntity;
