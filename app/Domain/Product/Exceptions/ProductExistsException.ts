class ProductExistsException extends Error{
    constructor(...args: any) {
        super(...args);
        Error.captureStackTrace(this, ProductExistsException)
    }
}

export default ProductExistsException;