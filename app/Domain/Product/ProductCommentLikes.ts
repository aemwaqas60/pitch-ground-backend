class ProductCommentLikesEntity {
    likesId: string;
    commentId: string;
    likes: string;
    constructor(likesId, commentId, likes,
    ) {

        this.likesId = likesId;
        this.commentId = commentId;
        this.likes = likes;

    }


    static createFromDetails(likesId, commentId, likes
    ) {

        return new ProductCommentLikesEntity(
            likesId,
            commentId,
            likes,
        );
    }


    static createFromObject(productCommentLikesOjb) {
        return new ProductCommentLikesEntity(
            productCommentLikesOjb.likesId,
            productCommentLikesOjb.commentId,
            productCommentLikesOjb.likes,
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));

    };

}


module.exports= ProductCommentLikesEntity;


