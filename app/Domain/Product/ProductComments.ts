class ProductCommentsEntity {
    commentId: string;
    productId: string;
    parentCommentId: string;
    comment: string;
    constructor(commentId, productId, parentCommentId, comment) {

        this.commentId = commentId;
        this.productId = productId;
        this.parentCommentId = parentCommentId;
        this.comment = comment;

    }


    static createFromDetails(commentId, productId, parentCommentId, comment, followes_email) {

        return new ProductCommentsEntity(
            commentId,
            productId,
            parentCommentId,
            comment,
        );
    }


    static createFromObject(productCommentOjb) {
        return new ProductCommentsEntity(
            productCommentOjb.commentId,
            productCommentOjb.productId,
            productCommentOjb.parentCommentId,
            productCommentOjb.comment,
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));

    };


}


module.exports = ProductCommentsEntity;

