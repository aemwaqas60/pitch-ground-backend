class ProductFollowingEntity {
    followingId: string;
    productId: string;
    userId: string;
    constructor(followingId, productId,userId) {

        this.followingId = followingId;
        this.productId = productId;
        this.userId = userId;
    }


    static createFromDetails(followingId,productId,userId) {

        return new ProductFollowingEntity(
            followingId,
            productId,
            userId
        );
    }


    static createFromObject(productFollowingObj) {
        return new ProductFollowingEntity(
            productFollowingObj.followingId,
            productFollowingObj.productId,
            productFollowingObj.userId,
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));
    };

}


module.exports = ProductFollowingEntity;
