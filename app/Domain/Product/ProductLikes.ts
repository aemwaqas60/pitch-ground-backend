class ProductLikesEntity {
    likeId: string;
    productId: string;
    likes: string;
    constructor(likeId, productId, likes,
    ) {

        this.likeId = likeId;
        this.productId = productId;
        this.likes = likes;

    }


    static createUserSetting(likeId, productId, likes
    ) {

        return new ProductLikesEntity(
            likeId,
            productId,
            likes,
        );
    }


    static createFromObject(productLikesOjb) {
        return new ProductLikesEntity(
            productLikesOjb.likeId,
            productLikesOjb.productId,
            productLikesOjb.likes,
        );

    }

    toObject() {
        return JSON.parse(JSON.stringify(this));

    };


}


module.exports = ProductLikesEntity;

