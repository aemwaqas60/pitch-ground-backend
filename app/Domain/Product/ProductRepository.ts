import Product from "./Products";

interface ProductRepository {
    /**
     *
     * @param {Product} product
     * @return Promise<boolean>
     */
    save(product: Product): Promise<boolean>;

    /**
     *
     * @param userId
     * @return {Product[]}
     */
    getByUserId(userId: string): Promise<Product[]>;

    /**
     *
     * @param productId
     * @return Product
     */
    getByProductId(productId: string): Promise<Product>;

    /**
     *
     * @param productName
     * @return {Product}
     */
    getByProductName(productName: string): Promise<object>;

    /**
     *
     * @param {Product} product
     * @return boolean
     */
    update(product: Product): Promise<boolean>;

    /**
     *
     * @param productId
     * @return boolean
     */
    remove(product: Product): Promise<boolean>;
}

export default ProductRepository;