import Platform from "../Core/Platform";
import Funding from "../Core/Funding";
import BusinessModel from "../Core/BusinessModel";
import Tag from "../Core/Tag";
import FounderSkillSet from "../Core/FounderSkillSet";
import PageAdmin from "../Core/PageAdmin";
import NumberOfEmployees from "../Core/NumberOfEmployees";
import NumberOfFounder from "../Core/NumberOfFounder";
import InitialCommitment from "../Core/InitialCommitment";
const InvalidDataException = require("../Core/Exceptions/InvalidDataException");

class Product {
    productId: string;
    userId: string;
    productName: string;
    logo: string;
    description: string;
    websiteLink: string;
    tagLine: string;
    motivation: string;
    twitterLink: string;
    facebookLink: string;
    startDate: Date;
    endDate: Date;
    location: string;
    pageAdmins: Array<PageAdmin>;
    numberOfEmployees: NumberOfEmployees;
    numberOfFounder: NumberOfFounder;
    initialCommitment: InitialCommitment;
    founderSkillSet: FounderSkillSet;
    businessModels: Array<BusinessModel>;
    fundings: Array<Funding>;
    platforms: Array<Platform>;
    tags: Array<Tag>;

    constructor(productId: string, userId: string, productName: string, logo: string, description: string, websiteLink: string, tagLine: string,
        motivation: string, twitterLink: string, facebookLink: string, startDate: Date, endDate: Date, location: string, numberOfEmployees: NumberOfEmployees,
        numberOfFounder: NumberOfFounder, initialCommitment: InitialCommitment, founderSkillSet: FounderSkillSet) {

        this.productId = productId;
        this.userId = userId;
        this.productName = productName;
        this.logo = logo;
        this.description = description;
        this.websiteLink = websiteLink;
        this.tagLine = tagLine;
        this.motivation = motivation;
        this.twitterLink = twitterLink;
        this.facebookLink = facebookLink;
        this.startDate = startDate;
        this.endDate = endDate;
        this.location = location;
        this.numberOfEmployees = numberOfEmployees;
        this.numberOfFounder = numberOfFounder;
        this.initialCommitment = initialCommitment;
        this.founderSkillSet = founderSkillSet;
        this.pageAdmins = [];
        this.businessModels = [];
        this.fundings = [];
        this.platforms = [];
        this.tags = [];
    }

    /**
     * Set Page Admins
     * @param {PageAdmin[]} pageAdmins
     */
    setPageAdmin(pageAdmins: PageAdmin[]): void {
        this.pageAdmins = pageAdmins;
    }

    /**
     * Set business models
     * @param {BusinessModel[]} businessModels
     */
    setBusinessModel(businessModels: BusinessModel[]): void {
        this.businessModels = businessModels;
    }

    /**
     * Set funding types for Product
     * @param {Funding[]} fundings
     */
    setFunding(fundings: Funding[]): void {
        this.fundings = fundings;
    }

    /**
     * Set supported platform for apps
     * @param {Platform} platforms
     */
    setPlatform(platforms: Platform[]): void {
        this.platforms = platforms;
    }

    /**
     * Set tags for product
     * @param {Tag} tags
     */
    setTags(tags: Tag[]): void {
        this.tags = tags;
    }

    /**
     *
     * @param {PageAdmin} pageAdmin
     */
    addPageAdmin(pageAdmin: PageAdmin): void {
        this.pageAdmins = [...this.pageAdmins, pageAdmin];
    }

    /**
     *
     * @param {BusinessModel} businessModel
     */
    addBusinessModel(businessModel: BusinessModel): void {
        this.businessModels = [...this.businessModels, businessModel];
    }

    /**
     *
     * @param {Funding} funding
     */
    addFunding(funding: Funding): void {
        this.fundings = [...this.fundings, funding]
    }

    /**
     *
     * @param {Platform} platform
     */
    addPlatform(platform: Platform): void {
        this.platforms = [...this.platforms, platform]
    }

    /**
     *
     * @param {Tag} tag
     */
    addTags(tag: Tag): void {
        this.tags = [...this.tags, tag]
    }

    toObject(): JSON {
        return JSON.parse(JSON.stringify(this));
    }

    toStoreObject(): object {
        return {
            productId: this.productId,
            userId: this.userId,
            productName: this.productName,
            logo: this.logo,
            description: this.description,
            websiteLink: this.websiteLink,
            tagLine: this.tagLine,
            motivation: this.motivation,
            twitterLink: this.twitterLink,
            facebookLink: this.facebookLink,
            startDate: this.startDate,
            endDate: this.endDate,
            location: this.location,
            numberOfEmployees: this.numberOfEmployees,
            numberOfFounder: this.numberOfFounder,
            initialCommitment: this.initialCommitment,
            founderSkillSet: this.founderSkillSet,
        }
    }

    static createFromObject(productObj: any): Product {
        return new Product(
            productObj.productId,
            productObj.userId,
            productObj.productName,
            productObj.logo,
            productObj.description,
            productObj.websiteLink,
            productObj.tagLine,
            productObj.motivation,
            productObj.twitterLink,
            productObj.facebookLink,
            productObj.startDate,
            productObj.endDate,
            productObj.location,
            productObj.founderSkillSet,
            productObj.numberOfEmployees,
            productObj.numberOfFounder,
            productObj.initialCommitment,
        );
    }


}

export default Product;


