//core models
import {ProductModel} from "../../Infrastructure/MysqlRepositories/DbProductRepository";
import {UserModel} from "../../Infrastructure/MysqlRepositories/DbUserRepository";
import {BusinessModelSchemaModel} from "../../Infrastructure/MysqlRepositories/DbBusinessModelRepository";
import {PageAdminModel} from "../../Infrastructure/MysqlRepositories/DAOs/PageAdminDAO";
import {FundingModel} from "../../Infrastructure/MysqlRepositories/DbFundingRepository";
import {PlatformModel} from "../../Infrastructure/MysqlRepositories/DbPlatformRepository";
import {TagModel} from "../../Infrastructure/MysqlRepositories/DbTagRepository";

//look up models
import {BusinessModelLookUpModel} from "../../Infrastructure/MysqlRepositories/DAOs/BusinessModelDAO";
import {FundingLookUpModel} from "../../Infrastructure/MysqlRepositories/DAOs/FundingDAO";
import {PlatformLookUpModel} from "../../Infrastructure/MysqlRepositories/DAOs/PlatformDAO";
import {TagLookUpModel} from "../../Infrastructure/MysqlRepositories/DAOs/TagDAO";

const Sequelize = require("sequelize");

class DbSchemaService {
    static async create(): Promise<boolean>{
        try {
            // UserModel.belongsTo(ProductModel);
            // ProductModel.belongsToMany(BusinessModelSchemaModel, { through: 'BusinessModelLookUps' });
            // BusinessModelLookUpModel.belongsToMany(ProductModel, { through: 'BusinessModelLookUps' });
            // ProductModel.belongsToMany(FundingModel, { through: 'FundingModelLookUps' });
            // FundingModel.belongsToMany(ProductModel, { through: 'FundingModelLookUps' });
            // ProductModel.belongsToMany(PlatformModel, { through: 'PlatformLookUps' });
            // PlatformModel.belongsToMany(ProductModel, { through: 'PlatformLookUps' });
            // ProductModel.belongsToMany(TagModel, { through: 'TagLookUps' });
            // TagModel.belongsToMany(ProductModel, { through: 'TagLookUps' });
            // UserModel.belongsTo(PageAdminModel, { through: 'PageAdmins' });
            // ProductModel.belongsTo(PageAdminModel, { through: 'PageAdmins' });


            // TagModel.associate = (models: any) => {
            //     TagModel.hasMany(models.TagLookUpModel, {as: 'tags', foreignKey: 'tagId'});
            // };
            //
            // ProductModel.associate = (models: any) => {
            //     ProductModel.belongsTo(models.UserModel, {foreignKey: 'userId'})
            // };
            UserModel.associate = (models: any) => {
                UserModel.hasMany(models.ProductModel, {foreignKey: 'userId'});
            };

            const productModelUp = await ProductModel.sync();
            const userModelUp = await UserModel.sync();
            const businessModelSchemaModelUp = await BusinessModelSchemaModel.sync();
            const pageAdminModelUp = await PageAdminModel.sync();
            const fundingModelUp = await FundingModel.sync();
            const platformModelUp = await PlatformModel.sync();
            const tagModelUp = await TagModel.sync();
            const businessModelLookUpModelUp = await BusinessModelLookUpModel.sync();
            const fundingLookUpModelUp = await FundingLookUpModel.sync();
            const platformLookUpModelUp = await PlatformLookUpModel.sync();
            const tagLookUpModelUp = await TagLookUpModel.sync();

            Promise.all([
                productModelUp,
                userModelUp,
                businessModelSchemaModelUp,
                pageAdminModelUp,
                fundingModelUp,
                platformModelUp,
                tagModelUp,
                businessModelLookUpModelUp,
                fundingLookUpModelUp,
                platformLookUpModelUp,
                tagLookUpModelUp,
            ]);
            return true;
        }catch {
            return false;
        }

    }

    static async drop(): Promise<boolean>{
        try{
            const productModelDrop = await ProductModel.destroy();
            const userModelDrop = await UserModel.destroy();
            const businessModelSchemaModelDrop = await BusinessModelSchemaModel.destroy();
            const pageAdminModelDrop = await PageAdminModel.destroy();
            const fundingModelDrop = await FundingModel.destroy();
            const platformModelDrop = await PlatformModel.destroy();
            const tagModelDrop = await TagModel.destroy();
            const businessModelLookUpModelDrop = await BusinessModelLookUpModel.destroy();
            const fundingLookUpModelDrop = await FundingLookUpModel.destroy();
            const platformLookUpModelDrop = await PlatformLookUpModel.destroy();
            const tagLookUpModelDrop = await TagLookUpModel.destroy();

            Promise.all([
                productModelDrop,
                userModelDrop,
                businessModelSchemaModelDrop,
                pageAdminModelDrop,
                fundingModelDrop,
                platformModelDrop,
                tagModelDrop,
                businessModelLookUpModelDrop,
                fundingLookUpModelDrop,
                platformLookUpModelDrop,
                tagLookUpModelDrop,
            ]);
            return true;
        }catch {
            return false;
        }
    }
}

export default DbSchemaService;