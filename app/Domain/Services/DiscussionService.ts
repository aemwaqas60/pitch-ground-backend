import Discussion from "../Discussion/Discussion";
import DbDiscussionRepository from "../../Infrastructure/MysqlRepositories/DbDiscussionRepository";
import DiscussionCreationException from "./Exceptions/DiscussionCreationException";
import DiscussionFetchException from "./Exceptions/DiscussionFetchException";
import DiscussionDeletionException from "./Exceptions/DiscussionDeletionException";
import DiscussionUpdateException from "./Exceptions/DiscussionUpdateException";

class DiscussionService {

    static async newDiscussion(discussionObj: any, userId: string): Promise<boolean>{
        try {
            const discussion = Discussion.createFromObject(discussionObj);
            discussion.setUserId(userId);
            return await DbDiscussionRepository.save(discussion);
        }catch {
            throw new DiscussionCreationException();
        }
    }

    static async getDiscussions(): Promise<Discussion[]>{
        try {
            return await DbDiscussionRepository.getAll();
        }catch {
            throw new DiscussionFetchException();
        }
    }

    static async getAllUserDiscussions(userId: string): Promise<Discussion[]>{
        try {
            return await DbDiscussionRepository.getByUserId(userId);
        }catch {
            throw new DiscussionFetchException();
        }
    }

    static async getUserSingleDiscussion(discussionId: string, userId: string): Promise<Discussion>{
        try {
            return await DbDiscussionRepository.getByDiscussionAndUserId(discussionId, userId);
        }catch {
            throw new DiscussionFetchException();
        }
    }

    static async updateDiscussion(discussion: Discussion): Promise<boolean>{
        try {
            return  await DbDiscussionRepository.update(discussion);
        }catch {
            throw new DiscussionUpdateException();
        }
    }

    static async removeDiscussion(discussion: Discussion): Promise<boolean>{
        try {
            return await DbDiscussionRepository.remove(discussion);
        }catch {
            throw new DiscussionDeletionException();
        }
    }

    static async removeAllUserDiscussions(userId: string): Promise<boolean>{
        try {
            return await DbDiscussionRepository.removeAll(userId);
        }catch {
            throw new DiscussionDeletionException();
        }
    }
}

export default DiscussionService;