class DiscussionCreationException extends Error{
    constructor(...args: any){
        super(...args);
        Error.captureStackTrace(this, DiscussionCreationException);
    }
}

export default DiscussionCreationException;
