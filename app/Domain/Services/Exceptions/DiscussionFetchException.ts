class DiscussionFetchException extends Error{
    constructor(...args: any){
        super(...args);
        Error.captureStackTrace(this, DiscussionFetchException);
    }
}

export default DiscussionFetchException;