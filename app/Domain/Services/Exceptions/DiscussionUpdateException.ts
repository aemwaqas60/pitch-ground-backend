class DiscussionUpdateException extends Error{
    constructor(...args: any){
        super(...args);
        Error.captureStackTrace(this, DiscussionUpdateException);
    }
}

export default DiscussionUpdateException;