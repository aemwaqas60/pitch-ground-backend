import DbProductRepository from "../../Infrastructure/MysqlRepositories/DbProductRepository";
import PageAdmin from "../Core/PageAdmin";
import Funding from "../Core/Funding";
import Platform from "../Core/Platform";
import BusinessModel from "../Core/BusinessModel";
import Tag from "../Core/Tag";
import Product from "../Product/Products";
import DbPageAdminRepository from "../../Infrastructure/MysqlRepositories/DbPageAdminRepository";
import DbBusinessModelRepository from "../../Infrastructure/MysqlRepositories/DbBusinessModelRepository";
import DbFundingRepository from "../../Infrastructure/MysqlRepositories/DbFundingRepository";
import DbTagRepository from "../../Infrastructure/MysqlRepositories/DbTagRepository";
import DbPlatformRepository from "../../Infrastructure/MysqlRepositories/DbPlatformRepository";
import ProductExistsException from "../Product/Exceptions/ProductExistsException";

class ProductService {
    static async getUserProducts(userId: string): Promise<any> {
        try {
            const userProducts = await DbProductRepository.getByUserId(userId);
            const userProductOps = userProducts.map(async userProduct => {
                const product = Product.createFromObject(userProduct);
                const pageAdmins = await DbPageAdminRepository.getByProductId(product.productId);
                const businessModels = await DbBusinessModelRepository.getByProductId(product.productId);
                const fundings = await DbFundingRepository.getByProductId(product.productId);
                const tags = await DbTagRepository.getByProductId(product.productId);
                const platforms = await DbPlatformRepository.getByProductId(product.productId);
                product.setPageAdmin(pageAdmins);
                product.setBusinessModel(businessModels);
                product.setFunding(fundings);
                product.setTags(tags);
                product.setPlatform(platforms);
                return product;
            });
            return await Promise.all(userProductOps);
        } catch {
            return false;
        }

    }

    static async newProduct(
        productObj: any,
        pageAdmins: PageAdmin[],
        fundings: Funding[],
        platforms: Platform[],
        businessModels: BusinessModel[],
        tags: Tag[]
    ): Promise<boolean> {
        try {
            const product = Product.createFromObject(productObj);
            const productByName = await DbProductRepository.getByProductName(product.productName);
            if (productByName) {
                throw new ProductExistsException();
            }
            businessModels.forEach(businessModel => {
                product.addBusinessModel(businessModel);
            });
            fundings.forEach(funding => {
                product.addFunding(funding);
            });
            platforms.forEach(platform => {
                product.addPlatform(platform);
            });
            tags.forEach(tags => {
                product.addTags(tags);
            });
            pageAdmins.forEach(pageAdmin => {
                product.addPageAdmin(pageAdmin);
            });

            await DbProductRepository.save(product);
            return true;

        } catch {
            return false;
        }
    }

    static async updateProductDetails(
        productObj: any,
        pageAdmins: Array<PageAdmin>,
        fundings: Array<Funding>,
        platforms: Array<Platform>,
        businessModels: Array<BusinessModel>,
        tags: Array<Tag>
    ): Promise<boolean> {
        try {
            const product = Product.createFromObject(productObj);
            const productByName = await DbProductRepository.getByProductName(product.productName);
            if (productByName) {
                throw new ProductExistsException();
            }
            businessModels.forEach(businessModel => {
                product.addBusinessModel(businessModel);
            });
            await DbBusinessModelRepository.remove(product.productId);
            fundings.forEach(funding => {
                product.addFunding(funding);
            });
            await DbFundingRepository.remove(product.productId);
            platforms.forEach(platform => {
                product.addPlatform(platform);
            });
            await DbPlatformRepository.remove(product.productId);
            tags.forEach(tags => {
                product.addTags(tags);
            });
            await DbTagRepository.remove(product.productId);
            pageAdmins.forEach(pageAdmin => {
                product.addPageAdmin(pageAdmin);
            });
            await DbPageAdminRepository.remove(product.productId);

            // @ts-ignore
            await DbProductRepository.update(product);

            return true;
        } catch {
            return false;
        }
    }

    static async deleteProduct(product: Product): Promise<boolean> {
        try {

            // @ts-ignore
            await DbProductRepository.remove(product);
            await DbBusinessModelRepository.remove(product.productId);

            await DbFundingRepository.remove(product.productId);

            await DbPlatformRepository.remove(product.productId);

            await DbTagRepository.remove(product.productId);

            await DbPageAdminRepository.remove(product.productId);
            return true;
        } catch {
            return false;
        }
    }

}

export default ProductService;
