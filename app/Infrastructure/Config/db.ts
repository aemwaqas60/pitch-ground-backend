require('dotenv').config();

const db = {

    SQLdb: {
        username: process.env.SQL_DB_USERNAME,
        password: process.env.SQL_DB_PASSWORD,
        database: process.env.SQL_DB_DATABASE,
        host: process.env.SQL_DB_HOST,
        dialect: process.env.SQL_DB_DIALECT,
        port: process.env.SQL_DB_PORT,
        loading: process.env.SQL_LOG_STATUS || false,
        seederStorage: process.env.SQL_SEED_STORAGE || 'sequelize',
        operatorsAliases: false,
    }

};

export default db;
