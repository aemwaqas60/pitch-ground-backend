require('dotenv').config();

module.exports = {
  "development": {
    "username": process.env.SQL_DB_USERNAME,
    "password": process.env.SQL_DB_PASSWORD,
    "database": process.env.SQL_DB_DATABASE,
    "host": process.env.SQL_DB_HOST,
    "dialect": process.env.SQL_DB_DIALECT
  },
  "test": {
    "username": process.env.SQL_DB_USERNAME,
    "password": process.env.SQL_DB_PASSWORD,
    "database": process.env.SQL_DB_DATABASE,
    "host": process.env.SQL_DB_HOST,
    "dialect": process.env.SQL_DB_DIALECT
  },
  "production": {
    "username": process.env.SQL_DB_USERNAME,
    "password": process.env.SQL_DB_PASSWORD,
    "database": process.env.SQL_DB_DATABASE,
    "host": process.env.SQL_DB_HOST,
    "dialect": process.env.SQL_DB_DIALECT
  }
};
