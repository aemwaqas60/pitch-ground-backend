import MysqlConn from "../../DbConnections/mysql";
import BusinessModel from "../../../Domain/Core/BusinessModel";
import {BusinessModelSchemaModel} from "../DbBusinessModelRepository";

const Sequelize = require("sequelize");
const BusinessModelLookUpModel = MysqlConn.define('BusinessModelLookUp', {
    businessModelId: {
        type: Sequelize.UUID
    },
    productId: {type: Sequelize.UUID},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});

class BusinessModelDAO {
    static async fetchProductBusinessModel(productId: string) {
        try {
            const productBusinessModels = await MysqlConn.query(
                `SELECT * from BusinessModelLookUps             
                    JOIN BusinessModels 
                    ON BusinessModelLookUps.businessModelId=BusinessModels.businessModelId 
                    where productId='${productId}';`);
            return productBusinessModels[0].map((productBusinessModel: any) => {
                return BusinessModel.createFromDetails(productBusinessModel.businessModelId, productBusinessModel.value);
            });
        } catch {
            return false;
        }
    }

    static async addBusinessModelAssoc(businessModelId: string, productId: string) {
        try {
            await BusinessModelLookUpModel.create({businessModelId, productId});
            return true;
        } catch {
            return false;
        }
    }

    static async removeAllProductBusinessModel(productId: string) {
        try {
            await BusinessModelLookUpModel.destroy({where: {productId}});
            return true;
        } catch (err) {
            return false;
        }
    }
}

export {BusinessModelLookUpModel};
export default BusinessModelDAO;