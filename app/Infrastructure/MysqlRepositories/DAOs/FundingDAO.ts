import MysqlConn from "../../DbConnections/mysql";
import Funding from "../../../Domain/Core/Funding";
import {FundingModel} from "../DbFundingRepository";

const Sequelize = require("sequelize");
const FundingLookUpModel = MysqlConn.define('FundingLookUp', {
    fundingId: {
        type: Sequelize.UUID
    },
    productId: {type: Sequelize.UUID},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});

class FundingDAO {
    static async fetchProductFunding(productId: string) {
        try {
            const productFundingsObj = await MysqlConn.query(
                `SELECT * from FundingLookUps             
                    JOIN Fundings 
                    ON FundingLookUps.fundingId=Fundings.fundingId 
                    where productId='${productId}';`);
            return productFundingsObj[0].map((productFundingObj: any) => {
                return Funding.createFromDetails(productFundingObj.fundingId, productFundingObj.value);
            });
        } catch {
            return false;
        }
    }

    static async addFundingAssoc(fundingId: string, productId: string) {
        try {
            await FundingLookUpModel.create({fundingId, productId});
            return true;
        } catch {
            return false;
        }
    }

    static async removeAllProductFunding(productId: string) {
        try {
            await FundingLookUpModel.destroy({where: {productId}});
            return true;
        } catch (err) {
            return false;
        }
    }
}

export {FundingLookUpModel};
export default FundingDAO;