import MysqlConn from "../../DbConnections/mysql";
import PageAdmin from "../../../Domain/Core/PageAdmin";
import User from "../../../Domain/Core/User";
import {UserModel} from "../DbUserRepository";
const Sequelize = require("sequelize");
const PageAdminModel = MysqlConn.define('PageAdmin', {
    pageAdminId: {type: Sequelize.UUID, primaryKey: true},
    userId: {type: Sequelize.UUID},
    productId: {type: Sequelize.UUID},
    role: {type: Sequelize.STRING},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});

class PageAdminDAO {
    static async fetchPageAdminsForProduct(productId: string) {
        try {
            const pageAdminsObj = await MysqlConn.query(
                `SELECT * from PageAdmins             
                    JOIN Users 
                    ON PageAdmins.userId=Users.userId 
                    where productId='${productId}';`);
            return pageAdminsObj[0].map((pageAdminObj: any) => {
                const pageAdmin = PageAdmin.createFromObject(pageAdminObj);
                const user = User.createFromObject(pageAdminObj);
                pageAdmin.setUser(user);
                return pageAdmin;
            });
        } catch {
            return false;
        }
    }

    /**
     *
     * @param {PageAdmin} pageAdmin
     */
    static async addPageAdminAssoc(pageAdmin: PageAdmin) {
        try {
            await PageAdminModel.create(pageAdmin.toStoreObject());
            return true;
        }catch {
            return false;
        }
    }

    static async removeAllPageAdminsForProduct(productId: string) {
        try {
            await PageAdminModel.destroy({where: {productId}});
            return true;
        } catch (err) {
            return false;
        }
    }
}

export {PageAdminModel}
export default PageAdminDAO;