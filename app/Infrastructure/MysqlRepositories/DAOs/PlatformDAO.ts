import MysqlConn from "../../DbConnections/mysql";
import Platform from "../../../Domain/Core/Platform";
import {PlatformModel} from "../DbPlatformRepository";

const Sequelize = require("sequelize");
const PlatformLookUpModel = MysqlConn.define('PlatformLookUp', {
    platformId: {type: Sequelize.UUID},
    productId: {type: Sequelize.UUID},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});

class PlatformDAO {

    static async fetchAllProductPlatforms(productId: string){
        try {
            const productPlatformsObj = await MysqlConn.query(
                `SELECT * from PlatformLookUps             
                    JOIN Platforms 
                    ON PlatformLookUps.platformId=Platforms.platformId 
                    where productId='${productId}';`);
            return productPlatformsObj[0].map((productPlatformObj: any) => {
                return Platform.createFromDetails(productPlatformObj.platformId, productPlatformObj.value);
            });
        } catch {
            return false;
        }
    }

    static async addPlatformAssoc(platformId: string, productId: string){
        try {
            await PlatformLookUpModel.create({platformId, productId});
            return true;
        }catch {
            return false;
        }
    }

    static async removeAllProductPlatforms(productId: string){
        try {
            await PlatformLookUpModel.destroy({where: {productId}});
            return true;
        } catch (err) {
            return false;
        }
    }
}

export {PlatformLookUpModel};
export default PlatformDAO;