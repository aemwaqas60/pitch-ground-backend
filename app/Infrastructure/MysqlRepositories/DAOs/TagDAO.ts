import MysqlConn from "../../DbConnections/mysql";
import Tag from "../../../Domain/Core/Tag";
import {TagModel} from "../DbTagRepository";
import sequelize = require("sequelize");

const Sequelize = require("sequelize");
const TagLookUpModel = MysqlConn.define('TagLookUps', {
    tagId: {type: Sequelize.UUID},
    productId: {type: Sequelize.UUID},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
}, {});
TagLookUpModel.associate = (models: any) => {
    TagLookUpModel.belongsTo(models.TagModel, {as: 'tag', foreignKey: 'tagId'})
};

class TagDAO {

    static async fetchAllProductTags(productId: string){
        try {
            const productTagsObj = await MysqlConn.query(
                `SELECT * from TagLookUps 
                    JOIN Tags 
                    ON TagLookUps.tagId=Tags.tagId 
                    where productId='${productId}';`);
            return productTagsObj[0].map((productTagObj: any) => {
                return Tag.createFromDetails(productTagObj.tagId, productTagObj.value);
            });
        } catch {
            return false;
        }
    }

    static async addTagAssoc(tagId: string, productId: string){
        try {
            await TagLookUpModel.create({tagId, productId});
            return true;
        }catch {
            return false;
        }
    }

    static async removeAllProductTags(productId: string){
        try {
            await TagLookUpModel.destroy({where: {productId}});
            return true;
        } catch (err) {
            return false;
        }
    }
}

export {TagLookUpModel};
export default TagDAO;