import MysqlConn from "../DbConnections/mysql";
import BusinessModelDAO from "./DAOs/BusinessModelDAO";
import BusinessModelRepository from "../../Domain/Core/BusinessModelRepository";
import BusinessModel from "../../Domain/Core/BusinessModel";

const Sequelize = require("sequelize");

const BusinessModelSchemaModel = MysqlConn.define('BusinessModel', {
    businessModelId: {
        type: Sequelize.UUID,
        primaryKey: true
    },
    value: {type: Sequelize.STRING},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});

class DbBusinessModelRepository implements BusinessModelRepository {
    async getByProductId(productId: string): Promise<BusinessModel[]> {

        return await BusinessModelDAO.fetchProductBusinessModel(productId);

    }

    async create(businessModels: BusinessModel[], productId: string): Promise<boolean> {
        try {
            await businessModels.map(async (businessModel: BusinessModel) => {
                return BusinessModelDAO.addBusinessModelAssoc(businessModel.businessModelId, productId);
            });
            return true;
        } catch {
            return false;
        }

    }

    async remove(productId: string): Promise<boolean> {
        try {
            await BusinessModelDAO.removeAllProductBusinessModel(productId);
            return true;
        } catch {
            return false;
        }

    }
}

export {BusinessModelSchemaModel};
export default new DbBusinessModelRepository();