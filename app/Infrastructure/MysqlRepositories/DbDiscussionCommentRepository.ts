import DiscussionCommentRepository from "../../Domain/Discussion/DiscussionCommentRepository";
import DiscussionComment from "../../Domain/Discussion/DiscussionComment";
import MysqlConn from "../DbConnections/mysql";
import DbOperationFailureException from "./Exceptions/DataException";
const Sequelize = require('sequelize');
const DiscussionCommentModel = MysqlConn.define('DiscussionComments', {
    discussionCommentId: {type: Sequelize.UUID, primaryKey: true},
    discussionId: {type: Sequelize.UUID},
    parentDiscussionCommentId: {type: Sequelize.UUID},
    comment: {type: Sequelize.STRING},
});
class DbDiscussionCommentRepository implements DiscussionCommentRepository{
    /**
     *
     * @param discussionComment
     */
    async save(discussionComment: DiscussionComment): Promise<boolean>{
        try {
            await DiscussionCommentModel.create(discussionComment);
            return true;
        }catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     *
     * @param discussionId
     */
    async getByDiscussionId(discussionId: string): Promise<DiscussionComment[]>{
        try {
            const discussionComments = await DiscussionCommentModel.findAll({where:{discussionId}});
            return discussionComments.map((discussionComment: object) => {
                return DiscussionComment.createFromObject(discussionComment);
            });
        }catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     *
     * @param discussionComment
     */
    async update(discussionComment: DiscussionComment): Promise<boolean>{
        try {
            await DiscussionCommentModel.update(discussionComment.toObject(), {
                where: {
                    discussionComment: discussionComment.discussionCommentId
                }
            });
            return true;
        }catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     *
     * @param discussionComment
     */
    async remove(discussionComment: DiscussionComment): Promise<boolean>{
        try {
            await DiscussionCommentModel.destroy({
                where: {
                    discussionComment: discussionComment.discussionCommentId
                }
            });
            return true;
        }catch {
            throw new DbOperationFailureException();
        }
    }
}

export {DiscussionCommentModel};
export default new DbDiscussionCommentRepository();