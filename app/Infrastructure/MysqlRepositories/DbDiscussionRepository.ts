import DiscussionRepository from "../../Domain/Discussion/DiscussionRepository";
import Discussion from "../../Domain/Discussion/Discussion";
import MysqlConn from "../DbConnections/mysql";
import DbOperationFailureException from "./Exceptions/DataException";
import User from "../../Domain/Core/User";

const Sequelize = require('sequelize');
const DiscussionModel = MysqlConn.define('Discussions', {
    discussionId: {type: Sequelize.UUID, primaryKey: true},
    title: {type: Sequelize.STRING},
    discussion: {type: Sequelize.STRING},
    userId: {type: Sequelize.UUID},
});

class DbDiscussionRepository implements DiscussionRepository {

    /**
     *
     * @param {Discussion} discussion
     */
    async save(discussion: Discussion): Promise<boolean> {
        try {
            await DiscussionModel.create(discussion.toStoreObject());
            return true;
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     * Fetch Discussions
     */
    async getAll(): Promise<Discussion[]> {
        try {
            const userDiscussions = await MysqlConn.query(`
                SELECT * FROM Discussions
                JOIN Users 
                ON Discussions.userId=Users.userId`
            );

            return userDiscussions[0].map((userDiscussion: object) => {
                const discussion = Discussion.createFromObject(userDiscussion);
                const user = User.createFromObject(userDiscussion);
                discussion.setUser(user);
                return discussion;
            });
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     *
     * @param userId
     */
    async getByUserId(userId: string): Promise<Discussion[]> {
        try {
            const userDiscussions = await MysqlConn.query(`
                SELECT * FROM Discussions
                JOIN Users 
                ON Discussions.userId=Users.userId
                WHERE Discussions.userId='${userId}'`
            );

            return userDiscussions[0].map((userDiscussion: object) => {
                const discussion = Discussion.createFromObject(userDiscussion);
                const user = User.createFromObject(userDiscussion);
                discussion.setUser(user);
                return discussion;
            });
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     *
     * @param discussionId
     * @param userId
     */
    async getByDiscussionAndUserId(discussionId: string, userId: string): Promise<Discussion> {
        try {
            const userDiscussion = await MysqlConn.query(`
                SELECT * FROM Discussions
                JOIN Users 
                ON Discussions.userId=Users.userId
                WHERE Discussions.userId='${userId}' AND discussionId='${discussionId}'`
            );

            const discussion = Discussion.createFromObject(userDiscussion[0][0]);
            const user = User.createFromObject(userDiscussion[0][0]);
            discussion.setUser(user);
            return discussion;
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     *
     * @param {Discussion} discussion
     */
    async update(discussion: Discussion): Promise<boolean> {
        try {
            await DiscussionModel.update(discussion.toObject(), {
                where: {
                    discussionId: discussion.discussionId
                }
            });
            return true;
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     *
     * @param discussion
     * @param {Discussion} discussion
     */
    async remove(discussion: Discussion): Promise<boolean> {
        try {
            await DiscussionModel.destroy({where: {discussionId: discussion.discussionId}});
            return true;
        } catch {
            throw new DbOperationFailureException();
        }
    }

    /**
     * Remove All User Discussions
     * @param userId
     */
    async removeAll(userId: string): Promise<boolean> {
        try {
            await DiscussionModel.destroy({where:{userId}});
            return true;
        } catch {
            throw new DbOperationFailureException();
        }
    }
}

export {DiscussionModel};
export default new DbDiscussionRepository();