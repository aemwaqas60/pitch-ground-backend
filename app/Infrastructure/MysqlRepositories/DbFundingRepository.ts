import MysqlConn from "../DbConnections/mysql";

import FundingDAO from "./DAOs/FundingDAO";
import Funding from "../../Domain/Core/Funding";
import FundingRepository from "../../Domain/Core/FundingRepository";

const Sequelize = require("sequelize");

const FundingModel = MysqlConn.define('Funding', {
    fundingId: {
        type: Sequelize.UUID,
        primaryKey: true
    },
    value: {type: Sequelize.STRING},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});

class DbFundingRepository implements FundingRepository {
    async getByProductId(productId: string): Promise<Funding[]> {
        return await FundingDAO.fetchProductFunding(productId);

    }

    async create(fundings: Funding[], productId: string): Promise<boolean> {
        try {
            await fundings.map(async (funding: Funding) => {
                return FundingDAO.addFundingAssoc(funding.fundingId, productId);
            });
            return true;
        } catch {
            return false;
        }

    }

    async remove(productId: string): Promise<boolean> {
        try {
            await FundingDAO.removeAllProductFunding(productId);
            return true;
        } catch {
            return false;
        }

    }
}

export {FundingModel};
export default new DbFundingRepository();
