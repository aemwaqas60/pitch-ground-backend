import PageAdminDAO from "./DAOs/PageAdminDAO";
import PageAdmin from "../../Domain/Core/PageAdmin";
import PageAdminRepository from "../../Domain/Core/PageAdminRepository";

class DbPageAdminRepository implements PageAdminRepository {
    async getByProductId(productId: string): Promise<PageAdmin[]> {
        return await PageAdminDAO.fetchPageAdminsForProduct(productId);

    }

    async create(pageAdmins: PageAdmin[]): Promise<boolean> {
        try {
            await pageAdmins.map(async (pageAdmin: PageAdmin) => {
                return PageAdminDAO.addPageAdminAssoc(pageAdmin);
            });
            return true;
        } catch {
            return false;
        }

    }

    async remove(productId: string): Promise<boolean> {
        try {
            await PageAdminDAO.removeAllPageAdminsForProduct(productId);
            return true;
        }catch {
            return false;
        }

    }
}

export default new DbPageAdminRepository();