import MysqlConn from "../DbConnections/mysql";

import PlatformRepository from "../../Domain/Core/PlatformRepository";
import Platform from "../../Domain/Core/Platform";
import PlatformDAO from "./DAOs/PlatformDAO";

const Sequelize = require("sequelize");

const PlatformModel = MysqlConn.define('Platform', {
    platformId: {
        type: Sequelize.UUID,
        primaryKey: true
    },
    value: {type: Sequelize.STRING},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});

class DbPlatformRepository implements PlatformRepository {
    async getByProductId(productId: string): Promise<Platform[]> {
        return await PlatformDAO.fetchAllProductPlatforms(productId);

    }

    async create(platforms: Platform[], productId: string): Promise<boolean> {
        try {
            await platforms.map(async (platform: Platform) => {
                return PlatformDAO.addPlatformAssoc(platform.platformId, productId);
            });
            return true;
        } catch {
            return false;
        }

    }

    async remove(productId: string): Promise<boolean> {
        try {
            await PlatformDAO.removeAllProductPlatforms(productId);
            return true;
        } catch {
            return false;
        }

    }
}

export {PlatformModel};
export default new DbPlatformRepository();