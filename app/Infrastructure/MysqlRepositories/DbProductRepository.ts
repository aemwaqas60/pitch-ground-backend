import MysqlConn from "../DbConnections/mysql";
import ProductRepository from "../../Domain/Product/ProductRepository";
import BusinessModelDAO from "./DAOs/BusinessModelDAO";
import FundingDAO from "./DAOs/FundingDAO";
import TagDAO from "./DAOs/TagDAO";
import PlatformDAO from "./DAOs/PlatformDAO";
import PageAdminDAO from "./DAOs/PageAdminDAO";
import Product from "../../Domain/Product/Products";

const Sequelize = require("sequelize");

const ProductModel = MysqlConn.define(
    'Product', {
        productId: {
            type: Sequelize.UUID,
            primaryKey: true
        },
        userId: {type: Sequelize.UUID},
        productName: {type: Sequelize.STRING},
        logo: {type: Sequelize.STRING},
        description: {type: Sequelize.STRING},
        websiteLink: {type: Sequelize.STRING},
        tagLine: {type: Sequelize.STRING},
        motivation: {type: Sequelize.STRING},
        twitterLink: {type: Sequelize.STRING},
        facebookLink: {type: Sequelize.STRING},
        startDate: {type: Sequelize.DATE},
        endDate: {type: Sequelize.DATE},
        location: {type: Sequelize.STRING},
        numberOfEmployees: {type: Sequelize.STRING},
        numberOfFounder: {type: Sequelize.STRING},
        initialCommitment: {type: Sequelize.STRING},
        founderSkillSet: {type: Sequelize.STRING},
        createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
        updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
    }, {}
);

class DbProductRepository implements ProductRepository {
    /**
     * Add User Product To Store
     * @param {Product} product
     */
    async save(product: Product): Promise<boolean> {
        try {
            await ProductModel.create(product.toStoreObject());
            const pageAdminAssoc = product.pageAdmins.map(async pageAdmin => {
                await PageAdminDAO.addPageAdminAssoc(pageAdmin);
            });
            const businessModelAssoc = product.businessModels.map(async businessModel => {
                await BusinessModelDAO.addBusinessModelAssoc(businessModel.businessModelId, product.productId);
            });
            const fundingAssoc = product.fundings.map(async funding => {
                await FundingDAO.addFundingAssoc(funding.fundingId, product.productId);
            });
            const platformAssoc = product.platforms.map(async platform => {
                await PlatformDAO.addPlatformAssoc(platform.platformId, product.productId);
            });
            const tagAssoc = product.tags.map(async tag => {
                await TagDAO.addTagAssoc(tag.tagId, product.productId);
            });
            const associations = pageAdminAssoc.concat(businessModelAssoc, fundingAssoc, platformAssoc, tagAssoc);
            await Promise.all(associations);
            return true;
        } catch {
            return false;
        }
    }

    /**
     * Get User Product By Product Id
     * @param productId
     */
    async getByProductId(productId: string): Promise<Product> {
        const product = await ProductModel.findOne({
            where: {
                productId
            }
        });
        return Product.createFromObject(product);
    }

    /**
     * Get User Product By User Id
     * @param userId
     */
    async getByUserId(userId: string): Promise<Product[]> {

        const products = await ProductModel.findAll({
            where: {
                userId
            }
        });
        return products.map((product: any) => {
                return Product.createFromObject(product);
            }
        );
    }

    /**
     * Get User Product By Product Name
     * @param productName
     */
    async getByProductName(productName: string): Promise<object> {

        return await ProductModel.findOne({
            where: {
                productName
            }
        });
    }

    /**
     * Update User Product
     * @param {Product} product
     */
    async update(product: Product): Promise<boolean> {
        try {

            await ProductModel.update(
                product.toObject(),
                {
                    where: {
                        productId: product.productId
                    }
                });
            const pageAdminAssoc = product.pageAdmins.map(async pageAdmin => {
                await PageAdminDAO.addPageAdminAssoc(pageAdmin);
            });

            const fundingAssoc = product.fundings.map(async funding => {
                await FundingDAO.addFundingAssoc(funding.fundingId, product.productId);
            });
            const businessModelAssoc = product.businessModels.map(async businessModel => {
                await BusinessModelDAO.addBusinessModelAssoc(businessModel.businessModelId, product.productId);
            });
            const platformAssoc = product.platforms.map(async platform => {
                await PlatformDAO.addPlatformAssoc(platform.platformId, product.productId);
            });
            const tagAssoc = product.tags.map(async tag => {
                await TagDAO.addTagAssoc(tag.tagId, product.productId);
            });
            const associations = pageAdminAssoc.concat(businessModelAssoc, fundingAssoc, platformAssoc, tagAssoc);
            await Promise.all(associations);
            return true;
        } catch {
            return false;
        }
    }

    /**
     * Remove User Product
     * @param {Product} product
     */
    async remove(product: Product): Promise<boolean> {
        try {
            await ProductModel.destroy({where: {productId: product.productId}});
            return true;
        } catch {
            return false;
        }
    }
}

export {ProductModel};
export default new DbProductRepository();