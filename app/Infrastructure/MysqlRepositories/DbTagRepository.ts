import MysqlConn from "../DbConnections/mysql";
import Tag from "../../Domain/Core/Tag";
import TagDAO, {TagLookUpModel} from "./DAOs/TagDAO";
import TagRepository from "../../Domain/Core/TagRepository";

const Sequelize = require("sequelize");

const TagModel = MysqlConn.define('Tag', {
    tagId: {
        type: Sequelize.UUID,
        primaryKey: true
    },
    value: {type: Sequelize.STRING},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});

TagModel.associate = (models: any) => {
    TagModel.hasMany(models.TagLookUpModel, {as: 'tag', foreignKey: 'tagId'})
};

class DbTagRepository implements TagRepository {
    async getByProductId(productId: string): Promise<Tag[]> {
        return await TagDAO.fetchAllProductTags(productId);
    }

    async create(tags: Tag[], productId: string): Promise<boolean> {
        try {
            await tags.map(async (tag: Tag) => {
                return TagDAO.addTagAssoc(tag.tagId, productId);
            });
            return true;
        } catch {
            return false;
        }

    }

    async remove(productId: string): Promise<boolean> {
        try {
            await TagDAO.removeAllProductTags(productId);
            return true;
        } catch {
            return false;
        }

    }
}

export {TagModel};
export default new DbTagRepository();
