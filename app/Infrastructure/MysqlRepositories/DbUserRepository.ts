import MysqlConn from "../DbConnections/mysql";

const Sequelize = require("sequelize");

const UserModel = MysqlConn.define('User', {
    userId: {
        type: Sequelize.UUID,
        primaryKey: true
    },
    fullName: {type: Sequelize.STRING},
    userName: {type: Sequelize.STRING},
    email: {type: Sequelize.STRING},
    password: {type: Sequelize.STRING},
    publicEmail: {type: Sequelize.STRING},
    twitterLink: {type: Sequelize.STRING},
    dateOfBirth: {type: Sequelize.DATE},
    location: {type: Sequelize.STRING},
    profilePic: {type: Sequelize.STRING},
    biography: {type: Sequelize.STRING},
    createdAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW},
    updatedAt: {type: Sequelize.DATE, defaultValue: Sequelize.NOW}
});


export {UserModel};
