class DataNotFoundException extends Error {
    constructor(...args) {
        super(...args);
        Error.captureStackTrace(this, DataNotFoundException)
    }
}

export default DataNotFoundException;