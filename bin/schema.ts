import * as program from "commander";
import DbSchemaService from "../app/Domain/Services/DbSchemaService";


program
    .version('1.0.0', '-v, --version')
    .description('DB Schema');

program
    .command('initialize')
    .alias('init')
    .description('Initialize the database schema')
    .action(async () => {
        const createTables = await DbSchemaService.create();
        console.log(createTables);
    });

program.parse(process.argv);