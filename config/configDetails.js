require('dotenv').config();

module.exports = {

    host: process.env.host,
    port: process.env.port,

    JWT_SECRET: process.env.JWT_SECRET,
};