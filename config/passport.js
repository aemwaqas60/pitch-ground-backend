const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const config = require('./googleConfig');

const authController = require('../controllers/auth');

passport.serializeUser((user, done) => {
    done(null, user);
});


passport.use(
    new GoogleStrategy({
        // options for google strategy
        clientID: config.google.CLIENT_ID,
        clientSecret: config.google.CLIENT_SECRET,
        callbackURL: '/api/v1/auth/google/redirect'
    }, (accessToken, refreshToken, profile, done) => {

        console.log("Inside google call back");
        console.log("user profile :", profile);

        // authController.google(profile);
        done(null,profile);

    })
);