import * as Sequelize from "sequelize";
import {QueryInterface} from "sequelize";
export default {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable('UserSettings', {
            settingId: {
                type: Sequelize.STRING
            },
            userId: {
                type: Sequelize.STRING
            },
            followersAlerts: {
                type: Sequelize.STRING
            },
            postsAlerts: {
                type: Sequelize.STRING
            },
            followersEmailNotification: {
                type: Sequelize.STRING
            },
            postsEmailNotification: {
                type: Sequelize.STRING
            },
            createdAt: {

                type: Sequelize.DATE
            },
            updatedAt: {

                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable('UserSettings');
    }
};