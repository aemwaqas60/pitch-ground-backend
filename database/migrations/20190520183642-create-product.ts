import * as Sequelize from "sequelize";
import {QueryInterface} from "sequelize";
export default {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable('Products', {

            productId: {type: Sequelize.STRING},
            userId: {type: Sequelize.STRING},
            productName: {type: Sequelize.STRING},
            logo: {type: Sequelize.STRING},
            description: {type: Sequelize.STRING},
            websiteLink: {type: Sequelize.STRING},
            tagLine: {type: Sequelize.STRING},
            motivation: {type: Sequelize.STRING},
            twitterLink: {type: Sequelize.STRING},
            facebookLink: {type: Sequelize.STRING},
            startDate: {type: Sequelize.DATE},
            endDate: {type: Sequelize.DATE},
            location: {type: Sequelize.STRING},
            numberOfEmployees: {type: Sequelize.STRING},
            numberOfFounder: {type: Sequelize.STRING},
            initialCommitment: {type: Sequelize.STRING},
            founderSkillSet: {type: Sequelize.STRING},
            createdAt: {

                type: Sequelize.DATE
            },
            updatedAt: {

                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable('Products');
    }
};