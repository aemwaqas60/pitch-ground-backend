import * as Sequelize from "sequelize";
import {QueryInterface} from "sequelize";
export default {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.createTable('Platforms', {

      platformId: {
        type: Sequelize.STRING
      },
      value: {
        type: Sequelize.STRING
      },
      createdAt: {

        type: Sequelize.DATE
      },
      updatedAt: {

        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface: QueryInterface) => {
    return queryInterface.dropTable('Platforms');
  }
};