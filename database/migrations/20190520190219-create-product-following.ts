import * as Sequelize from "sequelize";
import {QueryInterface} from "sequelize";
export default {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.createTable('ProductFollowings', {

      followingId: {
        type: Sequelize.STRING
      },
      productId: {
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.STRING
      },
      createdAt: {

        type: Sequelize.DATE
      },
      updatedAt: {

        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface: QueryInterface) => {
    return queryInterface.dropTable('ProductFollowings');
  }
};