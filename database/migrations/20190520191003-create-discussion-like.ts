import * as Sequelize from "sequelize";
import {QueryInterface} from "sequelize";
export default {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.createTable('DiscussionLikes', {

      likesId: {
        type: Sequelize.STRING
      },
      discussionId: {
        type: Sequelize.STRING
      },
      userId: {
        type: Sequelize.STRING
      },
      createdAt: {

        type: Sequelize.DATE
      },
      updatedAt: {

        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface: QueryInterface) => {
    return queryInterface.dropTable('DiscussionLikes');
  }
};