import * as Sequelize from "sequelize";
import {QueryInterface} from "sequelize";
export default {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable('HashTags', {

            tagId: {
                type: Sequelize.STRING
            },
            discussionId: {
                type: Sequelize.STRING
            },
            tag: {
                type: Sequelize.STRING
            },
            createdAt: {

                type: Sequelize.DATE
            },
            updatedAt: {

                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable('HashTags');
    }
};