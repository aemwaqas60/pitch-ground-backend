import * as Sequelize from "sequelize";
import {QueryInterface} from "sequelize";
export default {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable('Users', {

            userId: {type: Sequelize.STRING},
            fullName: {type: Sequelize.STRING},
            userName: {type: Sequelize.STRING},
            email: {type: Sequelize.STRING},
            password: {type: Sequelize.STRING},
            publicEmail: {type: Sequelize.STRING},
            twitterLink: {type: Sequelize.STRING},
            dateOfBirth: {type: Sequelize.DATE},
            location: {type: Sequelize.STRING},
            profilePic: {type: Sequelize.STRING},
            biography: {type: Sequelize.STRING},
            createdAt: {
                type: Sequelize.DATE
            },
            updatedAt: {
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable('Users');
    }
};