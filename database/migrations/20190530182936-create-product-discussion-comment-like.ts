import * as Sequelize from "sequelize";
import {QueryInterface} from "sequelize";

export = {
    up: (queryInterface: QueryInterface) => {
        return queryInterface.createTable('ProductDiscussionCommentLikes', {
            likeId: {
                type: Sequelize.STRING
            },
            commentId: {
                type: Sequelize.STRING
            },
            userId: {
                type: Sequelize.STRING
            },

            createdAt: {

                type: Sequelize.DATE
            },

            updatedAt: {

                type: Sequelize.DATE
            }
        });
    },

    down: (queryInterface: QueryInterface) => {
        return queryInterface.dropTable('ProductDiscussionCommentLikes');
    }
};
