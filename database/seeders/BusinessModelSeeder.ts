import {QueryInterface} from "sequelize";
import BusinessModel from "../../app/Domain/Core/BusinessModel";

const businessModels = [
    {
        businessModelId: '1',
        value: 'Commission',
        createdAt: new Date('YYYY-MM-DD HH:mm:ss'),
        updatedAt: new Date('YYYY-MM-DD HH:mm:ss')
    },
    {
        businessModelId: '2',
        value: 'Fee',
        createdAt: new Date('YYYY-MM-DD HH:mm:ss'),
        updatedAt: new Date('YYYY-MM-DD HH:mm:ss')
    }];
const businessModelObjArray = businessModels.map(businessModelObj => {
    return BusinessModel.createFromObject(businessModelObj);
});
export default {
    up: function (queryInterface: QueryInterface) {
        return queryInterface.bulkInsert('BusinessModels', businessModelObjArray, {});
    },
};