import {QueryInterface} from "sequelize";
import Funding from "../../app/Domain/Core/Funding";

const fundings = [{
    fundingId: '1', value: 'Accelerator',
    createdAt: new Date('YYYY-MM-DD HH:mm:ss'),
    updatedAt: new Date('YYYY-MM-DD HH:mm:ss')
}, {
    fundingId: '2', value: 'VC Funded',
    createdAt: new Date('YYYY-MM-DD HH:mm:ss'),
    updatedAt: new Date('YYYY-MM-DD HH:mm:ss')
}];
const fundingObjArray = fundings.map(fundingObj => {
    return Funding.createFromObject(fundingObj);
});

export default {
    up: function (queryInterface: QueryInterface) {
        return queryInterface.bulkInsert('Fundings', fundingObjArray, {});
    },
};