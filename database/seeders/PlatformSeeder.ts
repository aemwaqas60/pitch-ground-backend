import {QueryInterface} from "sequelize";
import Platform from "../../app/Domain/Core/Platform";

const platforms = [{
    platformId: '1', value: 'iOS',
    createdAt: new Date('YYYY-MM-DD HH:mm:ss'),
    updatedAt: new Date('YYYY-MM-DD HH:mm:ss')
}, {
    platformId: '2', value: 'Mac',
    createdAt: new Date('YYYY-MM-DD HH:mm:ss'),
    updatedAt: new Date('YYYY-MM-DD HH:mm:ss')
}];
const platformsObjArray = platforms.map(platformObj => {
    return Platform.createFromObject(platformObj);
});
export default {
    up: function (queryInterface: QueryInterface) {
        return queryInterface.bulkInsert('Platforms', platformsObjArray, {});
    },
};