import {QueryInterface} from "sequelize";
import Tag from "../../app/Domain/Core/Tag";

const tags = [{
    tagId: '1', value: 'B2B',
    createdAt: new Date('YYYY-MM-DD HH:mm:ss'),
    updatedAt: new Date('YYYY-MM-DD HH:mm:ss')
}, {
    tagId: '2', value: 'APIs',
    createdAt: new Date('YYYY-MM-DD HH:mm:ss'),
    updatedAt: new Date('YYYY-MM-DD HH:mm:ss')
}];
const tagsObjArray = tags.map(tagObj => {
    return Tag.createFromObject(tagObj);
});
export default {
    up: function (queryInterface: QueryInterface) {
        return queryInterface.bulkInsert('Tags', tagsObjArray, {});
    },
};