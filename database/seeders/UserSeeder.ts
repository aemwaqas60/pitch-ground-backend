import {QueryInterface} from "sequelize";
import User from "../../app/Domain/Core/User";

const users = [
    {
        userId: 'testUser123',
        fullName: 'Test User',
        userName: 'testuser',
        email: 'testuser@gmail.com',
        password: 'testing123',
        publicEmail: 'testuser@gmail.com',
        twitterLink: 'testuser/twitter',
        dateOfBirth: '2000-01-01',
        location: 'testLocation',
        profilePic: '/testuser/profilepic',
        biography: 'Testing User',
        createdAt: new Date('YYYY-MM-DD HH:mm:ss'),
        updatedAt: new Date('YYYY-MM-DD HH:mm:ss')
    }];
const usersObjArray = users.map(userObj => {
    return User.createFromObject(userObj);
});
export default {
    up: function (queryInterface: QueryInterface) {
        return queryInterface.bulkInsert('Users', usersObjArray, {});
    },
};