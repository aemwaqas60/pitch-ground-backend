# PitchGround V2 - Backend

Pitch ground is reboot of older version, now built with Node.js(Typescript) and DDD

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Node 10+
NPM 6.5.9+
Docker
Docker Compose

```

### Building

A step by step series of examples that tell you how to get a development env running

First we will start with initializing all dependencies

```
npm i
```

Next for setup we will mount our .env file

```
ln -s .env.example .env
```

At last we can build everything using 

```
npm run build
```


End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Currently we are primarily relying on functional test cases for domain services

First we need to setup mysql
 
```
docker-compose -p pitchground-backend -f ./ops/docker/docker-compose.yml up -d
```
 
Next we will create schema for all of our models
 
```
npm run schemaInit
```

Next we will need to seed predefined value objects to database
 
```
npx sequelize db:seed:all
```

Now everything is setup we can run our test by executing following

```
npm run tests
```

If you want to check coverage only 

```
npm run coverage
```

## Deployment

Placeholder for adding deployment notes
