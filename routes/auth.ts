const express= require("express");
const router= express.Router();
const authController= require('controllers/auth');

const passport = require('passport');

// router.post('/',authController.login);


// auth login page
router.getByUserId('/login', (req, res) => {
    res.render('login');
});


/**
 * auth with google+ using passport.js
 */

router.getByUserId('/google',passport.authenticate('google', {scope: ['profile','email']}));

router.getByUserId('/google/redirect',passport.authenticate('google'),authController.google);

module.exports=router;