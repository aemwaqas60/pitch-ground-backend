import DiscussionService from "../../../../app/Domain/Services/DiscussionService";
import { expect } from 'chai';
import 'mocha';
import Discussion from "../../../../app/Domain/Discussion/Discussion";

describe("Discussion Service", () => {

    const userId = "testUser123";

    describe("Add Discussion", () => {
        const discussionObj = {
            discussionId: 'test12323',
            discussion: "test discussion 1",
            title: "test title discussion 1",
        };

        const discussion2Obj = {
            discussionId: 'test1234',
            discussion: "test discussion 2",
            title: "test title discussion 2",
        };

        before(async function () {
            await DiscussionService.newDiscussion(discussionObj, userId);
            await DiscussionService.newDiscussion(discussion2Obj, userId);
        });

        describe("#getUserSingleDiscussion()", () => {
            const discussionId = 'test12323';
            it('should return Discussion', async function () {
                const discussion = await DiscussionService.getUserSingleDiscussion(discussionId, userId);
                expect(discussion).to.be.an('Object');
            });
        });

        describe("#getAllUserDiscussions()", () => {
            it('should return Discussion List', async function () {
                const discussions = await DiscussionService.getAllUserDiscussions(userId);
                expect(discussions.length).to.equals(2);
            });
        });
        describe("#getDiscussions()", () => {
            it('should return Discussion', async function () {
                const discussions = await DiscussionService.getDiscussions();
                expect(discussions.length).to.equals(2);
            });
        });
    });

    describe("Update Discussion", () => {
        const discussionObj = {
            discussionId: 'test12323',
            discussion: "test discussion updated 1",
            title: "test title discussion updated 1",
        };

        before(async function () {
            const discussion = Discussion.createFromObject(discussionObj);
            discussion.setUserId(userId);
            await DiscussionService.updateDiscussion(discussion);
        });

        describe("#getUserSingleDiscussion()", () => {
            const discussionId = 'test12323';
            it('should return Discussion', async function () {
                const discussion = await DiscussionService.getUserSingleDiscussion(discussionId, userId);
                expect(discussion).to.be.an('Object');
            });
        });

        context("Remove Discussion", () => {
            const discussionObj = {
                discussionId: 'test12323',
                discussion: "test discussion 1",
                title: "test title discussion 1",
            };
            it('should return true', async function () {
                const discussion = Discussion.createFromObject(discussionObj);
                discussion.setUserId(userId);
                const removeDiscussion = await DiscussionService.removeDiscussion(discussion);
                expect(removeDiscussion).to.equals(true);
            });
        });

        context("Remove All User Discussions", () => {
            it('should return true', async function () {
                const removeDiscussion = await DiscussionService.removeAllUserDiscussions(userId);
                expect(removeDiscussion).to.equals(true);
            });
        });
    });

});
