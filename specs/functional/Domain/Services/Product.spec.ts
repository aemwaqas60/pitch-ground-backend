import {expect} from 'chai';
import 'mocha';
import ProductService from "../../../../app/Domain/Services/ProductService";
import BusinessModel from "../../../../app/Domain/Core/BusinessModel";
import PageAdmin from "../../../../app/Domain/Core/PageAdmin";
import Funding from "../../../../app/Domain/Core/Funding";
import Platform from "../../../../app/Domain/Core/Platform";
import Tag from "../../../../app/Domain/Core/Tag";
import Product from "../../../../app/Domain/Product/Products";

describe('Product service', () => {
    describe('Add product', () => {
        const product = {
            productId: 'test1234',
            userId: 'testUser123',
            productName: 'test1234',
            logo: 'test',
            description: 'test',
            websiteLink: 'test',
            tagLine: 'test',
            motivation: 'test',
            twitterLink: 'test',
            facebookLink: 'test',
            startDate: '2019-05-01',
            endDate: '2019-05-01',
            location: 'test',
            numberOfEmployees: 'test',
            numberOfFounder: 'test',
            initialCommitment: 'test',
            founderSkillSet: 'test',
        };
        const pageAdmins = [{
            pageAdminId: '123',
            productId: 'test1234',
            userId: 'testUser123',
            role: 'co-founder'
        }];
        const businessModels = [{businessModelId: '1', value: 'Commission'}, {businessModelId: '2', value: 'Fee'}];
        const fundings = [{fundingId: '1', value: 'Accelerator'}, {fundingId: '2', value: 'VC Funded'}];
        const tags = [{tagId: '1', value: 'B2B'}, {tagId: '2', value: 'APIs'}];
        const platforms = [{platformId: '1', value: 'iOS'}, {platformId: '2', value: 'Mac'}];

        const userId = 'testUser123';

        before(async function () {
            const admins = pageAdmins.map(pageAdmin => {
                return new PageAdmin(pageAdmin.pageAdminId, pageAdmin.productId, pageAdmin.userId, pageAdmin.role);
            });

            const businessModel = businessModels.map(businessModel => {
                return new BusinessModel(businessModel.businessModelId, businessModel.value);
            });

            const funding = fundings.map(funding => {
                return new Funding(funding.fundingId, funding.value);
            });

            const platform = platforms.map(platform => {
                return new Platform(platform.platformId, platform.value);
            });

            const tag = tags.map(tag => {
                return new Tag(tag.tagId, tag.value);
            });

            await ProductService.newProduct(product, admins, funding, platform, businessModel, tag);
        });
        describe('#getUserProducts()', function () {
            it('should return list of product', async function () {
                const products = await ProductService.getUserProducts(userId);
                expect(products.length).to.equal(1);
                products.map((product: any) => {
                    expect(product.pageAdmins.length).to.equal(1);
                    expect(product.businessModels.length).to.equal(2);
                    expect(product.fundings.length).to.equal(2);
                    expect(product.platforms.length).to.equal(2);
                    expect(product.tags.length).to.equal(2);
                });
            });
        });

    });

    describe("Update product", function () {
        const productObj = {
            productId: 'test1234',
            userId: 'testUser123',
            productName: 'new',
            logo: 'updated',
            description: 'updated',
            websiteLink: 'updated',
            tagLine: 'updated',
            motivation: 'updated',
            twitterLink: 'updated',
            facebookLink: 'updated',
            startDate: '2019-05-28',
            endDate: '2019-05-28',
            location: 'test',
            numberOfEmployees: 'test',
            numberOfFounder: 'test',
            initialCommitment: 'test',
            founderSkillSet: 'test',
        };
        const pageAdmins = [{
            pageAdminId: '1',
            productId: 'test1234',
            userId: 'testUser123',
            role: 'co-founder'
        }];
        const businessModels = [{businessModelId: '1', value: 'Commission'}];
        const fundings = [{fundingId: '1', value: 'Accelerator'}];
        const tags = [{tagId: '1', value: 'B2B'}];
        const platforms = [{platformId: '2', value: 'Mac'}];
        const userId = 'testUser123';

        before(async function () {
            const businessModel = businessModels.map(businessModel => {
                return new BusinessModel(businessModel.businessModelId, businessModel.value);
            });
            const admins = pageAdmins.map(pageAdmin => {
                return new PageAdmin(pageAdmin.pageAdminId, pageAdmin.productId, pageAdmin.userId, pageAdmin.role);
            });
            const funding = fundings.map(funding => {
                return new Funding(funding.fundingId, funding.value);
            });
            const platform = platforms.map(platform => {
                return new Platform(platform.platformId, platform.value);
            });
            const tag = tags.map(tag => {
                return new Tag(tag.tagId, tag.value);
            });
            await ProductService.updateProductDetails(productObj, admins, funding, platform, businessModel, tag);
        });


        describe('#getUserProducts()', function () {
            it('should return list of product', async function () {
                const products = await ProductService.getUserProducts(userId);
                expect(products.length).to.equal(1);
                products.map((product: any) => {
                    expect(product.pageAdmins.length).to.equal(1);
                    expect(product.businessModels.length).to.equal(1);
                    expect(product.fundings.length).to.equal(1);
                    expect(product.platforms.length).to.equal(1);
                    expect(product.tags.length).to.equal(1);
                });
            });
        });

        context('delete product', function () {
            it('should return true', async function () {
                const product = Product.createFromObject(productObj);
                const deleteProduct = await ProductService.deleteProduct(product);
                expect(deleteProduct).to.equal(true);
            });
        });
    });
});

