import TagDAO from "../../../app/Infrastructure/MysqlRepositories/DAOs/TagDAO";
import {expect} from "chai";
import PageAdmin from "../../../app/Domain/Core/PageAdmin";
import BusinessModel from "../../../app/Domain/Core/BusinessModel";
import Funding from "../../../app/Domain/Core/Funding";
import Platform from "../../../app/Domain/Core/Platform";
import Tag from "../../../app/Domain/Core/Tag";
import ProductService from "../../../app/Domain/Services/ProductService";
import Product from "../../../app/Domain/Product/Products";

describe("Tag DAO", () => {
    describe('Add product', () => {
        const productObj = {
            productId: 'test1',
            userId: 'testUser123',
            productName: 'test1234',
            logo: 'test',
            description: 'test',
            websiteLink: 'test',
            tagLine: 'test',
            motivation: 'test',
            twitterLink: 'test',
            facebookLink: 'test',
            startDate: '2019-05-01',
            endDate: '2019-05-01',
            location: 'test',
            numberOfEmployees: 'test',
            numberOfFounder: 'test',
            initialCommitment: 'test',
            founderSkillSet: 'test',
        };
        const pageAdmins = [{
            pageAdminId: '123',
            productId: 'test1234',
            userId: 'testUser123',
            role: 'co-founder'
        }];

        const fundings = [{fundingId: '1', value: 'Accelerator'}, {fundingId: '2', value: 'VC Funded'}];
        const tags = [{tagId: '1', value: 'B2B'}, {tagId: '2', value: 'APIs'}];
        const platforms = [{platformId: '1', value: 'iOS'}, {platformId: '2', value: 'Mac'}];
        const businessModels = [{businessModelId: '1', value: 'Commission'}, {businessModelId: '2', value: 'Fee'}];

        before(async function () {

            const businessModel = businessModels.map(businessModel => {
                return new BusinessModel(businessModel.businessModelId, businessModel.value);
            });

            const funding = fundings.map(funding => {
                return new Funding(funding.fundingId, funding.value);
            });

            const platform = platforms.map(platform => {
                return new Platform(platform.platformId, platform.value);
            });

            const tag = tags.map(tag => {
                return new Tag(tag.tagId, tag.value);
            });

            const admins = pageAdmins.map(pageAdmin => {
                return new PageAdmin(pageAdmin.pageAdminId, pageAdmin.productId, pageAdmin.userId, pageAdmin.role);
            });

            await ProductService.newProduct(productObj, admins, funding, platform, businessModel, tag);
        });
        describe('#fetchAllProductTags()', function () {
            const productId = 'test1';
            it("should get List of Tags", async function () {
                const tags = await TagDAO.fetchAllProductTags(productId);
                expect(tags.length).to.equal(2);
            });
        });

        context('delete product', function () {
            it('should return true', async function () {
                const product = Product.createFromObject(productObj);
                const deleteProduct = await ProductService.deleteProduct(product);
                expect(deleteProduct).to.equal(true);
            });
        });
    });
});
